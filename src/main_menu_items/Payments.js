import React,{ useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import SvgIcon from '@material-ui/core/SvgIcon';
import { makeStyles } from '@material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles({
  appBar: {
    position: 'relative',
    cursor: 'pointer',
  },
  flex: {
    flex: 1,
  },
  paperStyle: {
    height: 100,
    width: 280,
    cursor: 'pointer',
  },
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

function MenuItemIcon(props) {
  return (
    <SvgIcon {...props} >
      <path d={props.iconD} />
    </SvgIcon>
  );
}

function Payments() {

  const [itemElevation, setElevation] = useState(2);
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const svgIconVal = "M12,2A10,10 0 0,1 22,12A10,10 0 0,1 12,22A10,10 0 0,1 2,12A10,10 0 0,1 12,2M12,4A8,8 0 0,0 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12A8,8 0 0,0 12,4M11,17V16H9V14H13V13H10A1,1 0 0,1 9,12V9A1,1 0 0,1 10,8H11V7H13V8H15V10H11V11H14A1,1 0 0,1 15,12V15A1,1 0 0,1 14,16H13V17H11Z";
  const label = "Payments & Billing";
  
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Paper className={classes.paperStyle} elevation={itemElevation} alignItems="center"
             onMouseOver={() => {setElevation(10)}}
             onMouseUp={() => setElevation(10)}
             onMouseLeave={() => setElevation(2)}
             onMouseDown={() => setElevation(2)}
             onClick={handleClickOpen}>
        <Grid container spacing={16} alignItems="center" direction="row">
          <Grid item>
            <Paper style={{margin: 10, marginLeft: 20}} >
              <MenuItemIcon iconD={svgIconVal}
                            color="secondary" style={{ fontSize: 40, height: 60, width: 65 }}/>
            </Paper>
          </Grid>
          <Grid item style={{padding: 10}}>
            <Typography variant="subtitle1" component="sub">{label}</Typography>
          </Grid>
        </Grid>
      </Paper>

      {/** Full Screen Dialog*/}
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar} onClick={handleClose} color="secondary">
          <Toolbar>
            <IconButton color="inherit" onClick={handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Grid container spacing={16} alignItems="center" direction="row">
              <Grid item>
                <MenuItemIcon iconD={svgIconVal}
                              color="default" style={{ fontSize: 30}}/>
              </Grid>
              <Grid item>
                <Typography variant="h6" color="inherit" className={classes.flex}>{label}</Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </Dialog>
    </div>
  );
}

Payments.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default Payments;