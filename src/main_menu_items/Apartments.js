import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import SvgIcon from '@material-ui/core/SvgIcon';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles } from '@material-ui/core/styles';
import { EditRounded, DeleteRounded} from '@material-ui/icons';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 500,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    appBar: {
        position: 'relative',
        cursor: 'pointer',
    },
    flex: {
        flex: 1,
    },
    paperStyle: {
        height: 100,
        width: 280,
        cursor: 'pointer',
    },
});

function Transition(props) {
    return <Slide direction="up" {...props} />;
}

function MenuItemIcon(props) {
    return (
        <SvgIcon {...props} >
            <path d={props.iconD} />
        </SvgIcon>
    );
}

function Apartments(props) {
    const [itemElevation, setElevation] = useState(2);
    const [open, setOpen] = React.useState(false);
    const {classes} = props;
    const svgIconVal = "M17,16H15V22H12V17H8V22H5V16H3L10,10L17,16M6,2L10,6H9V9H7V6H5V9H3V6H2L6,2M18,3L23,8H22V12H19V9H17V12H15.34L14,10.87V8H13L18,3Z";
    const label = "Apartment Units";

    function handleClickOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <div>
            <Paper className={classes.paperStyle} elevation={itemElevation} alignItems="center"
                onMouseOver={() => { setElevation(10) }}
                onMouseUp={() => setElevation(10)}
                onMouseLeave={() => setElevation(2)}
                onMouseDown={() => setElevation(2)}
                onClick={handleClickOpen}>
                <Grid container spacing={16} alignItems="center" direction="row">
                    <Grid item>
                        <Paper style={{ margin: 10, marginLeft: 20 }} >
                            <MenuItemIcon iconD={svgIconVal}
                                color="secondary" style={{ fontSize: 40, height: 60, width: 65 }} />
                        </Paper>
                    </Grid>
                    <Grid item style={{ padding: 10 }}>
                        <Typography variant="subtitle1" component="sub">
                            {label}
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>

            {/** Full Screen Dialog*/}
            <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
                <AppBar className={classes.appBar} onClick={handleClose} color="secondary">
                    <Toolbar>
                        <IconButton color="inherit" onClick={handleClose} aria-label="Close">
                            <CloseIcon />
                        </IconButton>
                        <Grid container spacing={16} alignItems="center" direction="row">
                            <Grid item>
                                <MenuItemIcon iconD={svgIconVal}
                                    color="default" style={{ fontSize: 30 }} />
                            </Grid>
                            <Grid item>
                                <Typography variant="h6" color="inherit" className={classes.flex}> {label} </Typography>
                            </Grid>
                        </Grid>
                    </Toolbar>
                </AppBar>
                <SimpleTable />
            </Dialog>
        </div>
    );

    function SimpleTable(props) {
    
        let id = 0;
        function createData(name, description) {
            id += 1;
            return { id, name, description };
        }
    
        const rows = [
            createData('Apartment 1A', 'Studio Unit'),
            createData('Apartment 1B', 'Studio Unit'),
            createData('Apartment 1C', 'Studio Unit'),
            createData('Apartment 2A', '1 Bedroom Unit'),
            createData('Apartment 2B', '1 Bedroom Unit'),
            createData('Apartment 2C', '1 Bedroom Unit'),
        ];
    
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead className={classes.thead}>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell align="right">Name</TableCell>
                            <TableCell align="right">Description</TableCell>
                            <TableCell align="center">Action</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map(row => (
                            <TableRow key={row.id} className={classes.row} hover>
                                <TableCell component="th" scope="row">{row.id}</TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.description}</TableCell>
                                <TableCell align="center">
                                    <IconButton color="primary" aria-label="Edit">
                                        <EditRounded/>
                                    </IconButton>      
                                    <IconButton color="secondary" aria-label="Delete">
                                        <DeleteRounded/>
                                    </IconButton>     
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        );
    }


}

Apartments.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Apartments);