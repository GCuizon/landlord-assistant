import React,{ useState } from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import SvgIcon from '@material-ui/core/SvgIcon';
import { makeStyles } from '@material-ui/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

const useStyles = makeStyles({
  appBar: {
    position: 'relative',
    cursor: 'pointer',
  },
  flex: {
    flex: 1,
  },
  paperStyle: {
    height: 100,
    width: 280,
    cursor: 'pointer',
  },
});

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

function MenuItemIcon(props) {
  return (
    <SvgIcon {...props} >
      <path d={props.iconD} />
    </SvgIcon>
  );
}

function Tenants() {

  const [itemElevation, setElevation] = useState(2);
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();
  const svgIconVal = "M9,4A4,4 0 0,1 13,8A4,4 0 0,1 9,12A4,4 0 0,1 5,8A4,4 0 0,1 9,4M9,6A2,2 0 0,0 7,8A2,2 0 0,0 9,10A2,2 0 0,0 11,8A2,2 0 0,0 9,6M9,13C11.67,13 17,14.34 17,17V20H1V17C1,14.34 6.33,13 9,13M9,14.9C6.03,14.9 2.9,16.36 2.9,17V18.1H15.1V17C15.1,16.36 11.97,14.9 9,14.9M15,4A4,4 0 0,1 19,8A4,4 0 0,1 15,12C14.53,12 14.08,11.92 13.67,11.77C14.5,10.74 15,9.43 15,8C15,6.57 14.5,5.26 13.67,4.23C14.08,4.08 14.53,4 15,4M23,17V20H19V16.5C19,15.25 18.24,14.1 16.97,13.18C19.68,13.62 23,14.9 23,17Z";
  const label = "Tenants";
  
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <div>
      <Paper className={classes.paperStyle} elevation={itemElevation} alignItems="center"
             onMouseOver={() => {setElevation(10)}}
             onMouseUp={() => setElevation(10)}
             onMouseLeave={() => setElevation(2)}
             onMouseDown={() => setElevation(2)}
             onClick={handleClickOpen}>
        <Grid container spacing={16} alignItems="center" direction="row">
          <Grid item>
            <Paper style={{margin: 10, marginLeft: 20}} >
              <MenuItemIcon iconD={svgIconVal}
                            color="secondary" style={{ fontSize: 40, height: 60, width: 65 }}/>
            </Paper>
          </Grid>
          <Grid item style={{padding: 10}}>
            <Typography variant="subtitle1" component="sub">{label}</Typography>
          </Grid>
        </Grid>
      </Paper>

      {/** Full Screen Dialog*/}
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar} onClick={handleClose} color="secondary">
          <Toolbar>
            <IconButton color="inherit" onClick={handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
            <Grid container spacing={16} alignItems="center" direction="row">
              <Grid item>
                <MenuItemIcon iconD={svgIconVal}
                              color="default" style={{ fontSize: 30}}/>
              </Grid>
              <Grid item>
                <Typography variant="h6" color="inherit" className={classes.flex}>{label}</Typography>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </Dialog>
    </div>
  );
}

Tenants.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default Tenants;