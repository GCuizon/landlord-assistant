import React from 'react';
import MyAppBar from '../components/MyAppBar';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import Apartments from '../main_menu_items/Apartments';
import Tenants from '../main_menu_items/Tenants';
import Payments from '../main_menu_items/Payments';
import Notifications from '../main_menu_items/Notifications';

const styles = theme => ({
    root: {
      paddingTop: theme.spacing.unit * 4,
      paddingBottom: theme.spacing.unit * 4,
    },
    paperStyle: {
      height: 100,
      width: 280,
      cursor: 'pointer',
    },
  });

class Dashboard extends React.Component {
  
    render() {
        const { classes } = this.props;

        return (
           <div>
               <MyAppBar/>
                <div>
                    <Grid container direction="row" className={classes.root} justify="center" alignItems="center" spacing={'16'}>
                        <Grid item>
                            <Apartments/>
                        </Grid>
                        <Grid item>
                            <Tenants/>
                        </Grid>
                        <Grid item>
                            <Payments/>
                        </Grid>
                        <Grid item>
                            <Notifications/>
                        </Grid>
                    </Grid>
                </div>
               
           </div>
        );
    }
}

export default withStyles(styles) (Dashboard);